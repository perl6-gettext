NAME
====

Gettext - Perl6 binding for [GNU Gettext](http://www.gnu.org/software/gettext/)

SYNOPSIS
========

    use Gettext;

DESCRIPTION
===========

Gettext is ...

AUTHOR
======

Matias Linares <matiaslina@gmail.com>

COPYRIGHT AND LICENSE
=====================

Copyright 2018 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

