use v6.d;
unit module Gettext:ver<0.0.1>;

use NativeCall;

enum LocaleCategory is export (
    # Character classification and case conversion.
    LcCType => 0,
    # Non-monetary numeric formats.
    LcNumeric => 1,
    # Date and time formats.
    LcTime => 2,
    # Collation order.
    LcCollate => 3,
    # Monetary formats.
    LcMonetary => 4,
    # Formats of informative and diagnostic messages and interactive responses.
    LcMessages => 5,
    # For all.
    LcAll => 6,
    # Paper size.
    LcPaper => 7,
    # Name formats.
    LcName => 8,
    # Address formats and location information.
    LcAddress => 9,
    # Telephone number formats.
    LcTelephone => 10,
    # Measurement units (Metric or Other).
    LcMeasurement => 11,
    # Metadata about the locale information.
    LcIdentification => 12,
);

our sub gettext(Str is encoded('utf8') --> Str) is native { … }
sub bindtextdomain(Str:D, Str:D --> Str) is native is export {…}
sub textdomain(Str:D --> Str) is native is export { … }
sub setlocale(int32, Str:D --> Str ) is native is export { … } 

sub _(Str:D $a --> Str) is export { gettext($a) }


=begin pod

=head1 NAME

Gettext - Perl6 binding for L<GNU Gettext|http://www.gnu.org/software/gettext/>

=head1 SYNOPSIS

  use Gettext;

=head1 DESCRIPTION

Gettext is ...

=head1 AUTHOR

Matias Linares <matiaslina@gmail.com>

=head1 COPYRIGHT AND LICENSE

Copyright 2018 Matias Linares

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
