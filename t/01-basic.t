use v6.c;
use Test;
use Gettext;

constant spanish = 'es_ES.utf8';
constant english = 'en_US.utf8';
constant text-domain = 'test';

ok bindtextdomain(text-domain, 't/langs/');
ok textdomain(text-domain);

setlocale(Gettext::LocaleCategory::LcAll, spanish);
is _('Hello, world!'), 'Hola, mundo!';

setlocale(Gettext::LocaleCategory::LcAll, english);
is _('Hello, world!'), 'Hello, world!';

done-testing;
